#import base64

from _bsspeke_cffi import ffi, lib

class Server:
    def __init__(self, server_id, user_id):
        sid_utf8 = server_id.encode('utf-8')
        print("sid_utf8:", sid_utf8.decode('utf-8'), len(sid_utf8))
        uid_utf8 = user_id.encode('utf-8')
        print("uid_utf8:", uid_utf8.decode('utf-8'), len(uid_utf8))

        from _bsspeke_cffi import ffi, lib
        self.ffi = ffi
        self.lib = lib
        
        self.ctx = self.ffi.new("bsspeke_server_ctx *")
        self.lib.bsspeke_server_init(self.ctx,
                                     sid_utf8, len(sid_utf8),
                                     uid_utf8, len(uid_utf8))

    def blind_salt(self, blind, salt):
        blind_salt = bytes(32)
        self.lib.bsspeke_server_blind_salt(blind_salt, blind, salt, len(salt))
        return blind_salt

    def generate_B(self, P):
        self.lib.bsspeke_server_generate_B(P, self.ctx)
        B = bytes(self.ffi.buffer(self.ctx.B, 32))
        return B

    def derive_shared_key(self, A, V):
        self.lib.bsspeke_server_derive_shared_key(A, V, self.ctx)
        
    def verify_client(self, client_verifier):
        rc = self.lib.bsspeke_server_verify_client(client_verifier, self.ctx)
        if rc != 0:
            return False
        else:
            return True

    def generate_verifier(self):
        server_verifier = bytes(32)
        self.lib.bsspeke_server_generate_verifier(server_verifier, self.ctx)
        return server_verifier

