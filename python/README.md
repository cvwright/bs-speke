This package provides a Python interface for speaking the BS-SPEKE
protocol for augmented password-authenticated key agreement (aPAKE).
