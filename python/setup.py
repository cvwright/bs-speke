from setuptools import setup

setup(
    name='BSSpeke',
    version='0.1.0',
    author='Charles Wright',
    author_email='cvwright@futo.org',
    packages=['BSSpeke'],
    #scripts=['bin/script1','bin/script2'],
    url='http://pypi.python.org/pypi/BS-SPEKE/',
    license='LICENSE.txt',
    description='An awesome package that does something',
    long_description=open('README.md').read(),
    setup_requires=["cffi>=1.0.0"],
    cffi_modules=["bsspeke_build.py:ffibuilder"],
    install_requires=["cffi>=1.0.0"],
)
