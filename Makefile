default: demo

lib: libbsspeke.a

minimonocypher.o: minimonocypher.c minimonocypher.h
	$(CC) -c minimonocypher.c -fPIC

bsspeke.o: bsspeke.c bsspeke.h minimonocypher.h
	$(CC) -c bsspeke.c -fPIC

demo.o: demo.c bsspeke.h
	$(CC) -c demo.c -fPIC

demo: demo.o libbsspeke.a
	$(CC) -o demo demo.o -L. -lbsspeke

libbsspeke.a: bsspeke.o minimonocypher.o
	ar rcs libbsspeke.a bsspeke.o minimonocypher.o

clean:
	rm -f demo bsspeke.o minimonocypher.o libbsspeke.a
